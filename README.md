# OReilly-JS-DataStructures
My version of solutions for the OReilly book. ISBN: 978-1-449-36493-9 

These solutions are for the end of the chapter exercises. Rather then implementing pure data structures, I want to use them in a more applied approach and solve problems.
