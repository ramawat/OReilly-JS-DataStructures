/* Write a function that inserts an element into a list only if the element to be inserted
is larger than any of the elements currently in the list. Larger can mean either greater
than when working with numeric values, or further down in the alphabet, when
working with textual values. */

/* List Data Structure Implementation from the Textbook */

function List(){
	this.listSize = 0;						// # elements in the List
	this.pos = 0;									// Current position in List
	this.dataStore = []; 					// initializes an empty array to store list elements
	this.clear = clear;						// Clears all elements from List
	this.find = find;							// Finds the position of element from List
	this.toString = toString;			// View elements of the List
	this.insert = insert;					// Insert an element at a specified position
	this.append = append;					// Add in list in front and resize
	this.remove = remove;					// Remove element from list and resize
	this.front = front;						// Front of List	
	this.end = end;								// End of List
	this.prev = prev;							// Previous Element
	this.next = next;							// Next Element
	this.length = length;					// Finds the length of the list
	this.currPos = currPos;				// Current position
	this.moveTo = moveTo;					// Move to specified position
	this.getElement = getElement;	// gets element at a position
	this.length = length;					// length of the list
	this.contains = contains;			// Check if element is in List
	this.insertElem = insertElem;

	function append(element){
		this.dataStore[this.listSize++] = element;
	}

	function find(element){
		for (var i = 0; i < this.dataStore.length; i++) {
			if(this.dataStore[i] == element){
				return i;
			}
		}
		return -1; // If not found 
	}

	function remove(element){
		var index = this.find(element);
		if(index > -1){
			this.dataStore.splice(index,1);
			--this.listSize;
			return true;
		}
		return false;
	}

	function length(){
		return this.listSize;
	}

	function toString(){
		return this.dataStore;
	}

	function insert(element){
		if(this.currPos() > -1){
			this.dataStore.splice(this.currPos()+1,0,element);
			++this.listSize;
			return true;
		}
		return false;
	}

	function clear(){
		delete this.dataStore;
		this.dataStore=[];
		this.listSize = this.pos = 0;
	}

	function contains(element){
		for (var i = 0; i < this.dataStore.length; i++) {
			if(this.dataStore[i] === element){
				return true;
			}
			return false;
		}
	}

	function front(){
		this.pos = 0;
	}

	function end(){
		this.pos = this.listSize-1;
	}

	function prev(){
		if(this.pos > 0){
			--this.pos;
		}
	}

	function next(){
		if(this.pos > 0){
			++this.pos;
		}
	}

	function currPos(){
		return this.pos;
	}

	function moveTo(position){
		this.pos = position;
	}

	function getElement(){
		return this.dataStore[this.pos];
	}

	function insertElem (element){
		for (this.front(); this.currPos() < this.length(); this.next())         {
			if (element > this.dataStore[this.currPos()]){
				this.dataStore.splice(this.currPos()+1,0,element);
			    ++this.listSize;
			    return true;
			}
			return false;
		}
	}

}



var testList = new List();
testList.append(2);
testList.append(5);
testList.append(7);
testList.append(14);
testList.append(4);

console.log(testList.toString());

testList.insertElem(1);
console.log(testList.toString());

testList.insertElem(3);
console.log(testList.toString());

testList.insertElem(6);
console.log(testList.toString());

/* 
[ 2, 5, 7, 14, 4 ]
[ 2, 5, 7, 14, 4 ]
[ 2, 3, 5, 7, 14, 4 ]
[ 2, 6, 3, 5, 7, 14, 4 ] */


var testList2 = new List();
testList2.append("Cynthia");
testList2.append("Ana");
testList2.append("Robert");
testList2.append("David");
testList2.append("Penny");

console.log(testList2.toString());

testList2.insertElem(" ");
console.log(testList2.toString());

testList2.insertElem("Bernelle");
console.log(testList2.toString());

testList2.insertElem("Emma");
console.log(testList2.toString());

/*
[ 'Cynthia', 'Ana', 'Robert', 'David', 'Penny' ]
[ 'Cynthia', 'Ana', 'Robert', 'David', 'Penny' ]
[ 'Cynthia', 'Ana', 'Robert', 'David', 'Penny' ]
[ 'Cynthia', 'Emma', 'Ana', 'Robert', 'David', 'Penny' ]*/

