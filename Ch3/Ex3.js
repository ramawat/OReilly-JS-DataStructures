/*
Create a Person class that stores a person's name and their gender. Create a list of at least 10 Person objects. Write a fucntion that displays all people in the list of the same gender.
 */

 /* List Data Structure Implementation from the Textbook */

 function List(){
	this.listSize = 0;						// # elements in the List
	this.pos = 0;									// Current position in List
	this.dataStore = []; 					// initializes an empty to store list elements
	this.clear = clear;						// Clears all elements from List
	this.find = find;							// Finds the position of element from List
	this.toString = toString;			// View elements of the List
	this.insert = insert;					// Insert an element at a specified position
	this.append = append;					// Add in list in front and resize
	this.remove = remove;					// Remove element from list and resize
	this.front = front;						// Front of List	
	this.end = end;								// End of List
	this.prev = prev;							// Previous Element
	this.next = next;							// Next Element
	this.length = length;					// Finds the length of the list
	this.currPos = currPos;				// Current position
	this.moveTo = moveTo;					// Move to specified position
	this.getElement = getElement;	// gets element at a position
	this.length = length;					// length of the list
	this.contains = contains;			// Check if element is in List

	function append(element){
		this.dataStore[this.listSize++] = element;
	}

	function find(element){
		for (var i = 0; i < this.dataStore.length; i++) {
			if(this.dataStore[i] == element){
				return i;
			}
		}
		return -1; // If not found 
	}

	function remove(element){
		var index = this.find(element);
		if(index > -1){
			this.dataStore.splice(index,1);
			--this.listSize;
			return true;
		}
		return false;
	}

	function length(){
		return this.listSize;
	}

	function toString(){
		return this.dataStore;
	}

	function insert(element, indexAfter){
		var insertPos = this.find(indexAfter);
		if(insertPos > -1){
			this.dataStore.splice(insertPos+1,0,element);
			++this.listSize;
			return true;
		}
		return false;
	}

	function clear(){
		delete this.dataStore;
		this.dataStore=[];
		this.listSize = this.pos = 0;
	}

	function contains(element){
		for (var i = 0; i < this.dataStore.length; i++) {
			if(this.dataStore[i] === element){
				return true;
			}
			return false;
		};
	}

	function front(){
		this.pos = 0;
	}

	function end(){
		this.pos = this.listSize-1;
	}

	function prev(){
		if(this.pos > 0){
			--this.pos;
		}
	}

	function next(){
		if(this.pos > 0){
			++this.pos;
		}
	}

	function currPos(){
		return this.pos;
	}

	function moveTo(position){
		this.pos = position;
	}

	function getElement(){
		return this.dataStore[this.pos];
	}
}

function Person(){
    this.name = "name";
    this.gender = "gender";

    function displayByGender(gender){
        this.dataStore.forEach((person)=>{
            if(person.gender===gender){
                return person.name;
            }
        });
    }
}

var P1 = new Person("P1","Male");
var P2 = new Person("P2","Male");
var P3 = new Person("P3","Female");
var P4 = new Person("P4","Female");
var P5 = new Person("P5","Male");
var P6 = new Person("P6","Male");
var P7 = new Person("P7","Female");
var P8 = new Person("P8","Male");
var P9 = new Person("P9","Female");
var P0 = new Person("P0","Male");

var testList = new List(Person());
testList.append(P1);
testList.append(P2);
testList.append(P3);
testList.append(P4);
testList.append(P5);
testList.append(P6);
testList.append(P7);
testList.append(P8);
testList.append(P9);
testList.append(P0);

console.log(testList.toString());
/* WIP */
