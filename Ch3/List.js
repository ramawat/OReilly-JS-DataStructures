/* List Data Structure Implementation from the Textbook */

function List(){
	this.listSize = 0;						// # elements in the List
	this.pos = 0;									// Current position in List
	this.dataStore = []; 					// initializes an empty to store list elements
	this.clear = clear;						// Clears all elements from List
	this.find = find;							// Finds the position of element from List
	this.toString = toString;			// View elements of the List
	this.insert = insert;					// Insert an element at a specified position
	this.append = append;					// Add in list in front and resize
	this.remove = remove;					// Remove element from list and resize
	this.front = front;						// Front of List	
	this.end = end;								// End of List
	this.prev = prev;							// Previous Element
	this.next = next;							// Next Element
	this.length = length;					// Finds the length of the list
	this.currPos = currPos;				// Current position
	this.moveTo = moveTo;					// Move to specified position
	this.getElement = getElement;	// gets element at a position
	this.length = length;					// length of the list
	this.contains = contains;			// Check if element is in List

	function append(element){
		this.dataStore[this.listSize++] = element;
	}

	function find(element){
		for (var i = 0; i < this.dataStore.length; i++) {
			if(this.dataStore[i] == element){
				return i;
			}
		}
		return -1; // If not found 
	}

	function remove(element){
		var index = this.find(element);
		if(index > -1){
			this.dataStore.splice(index,1);
			--this.listSize;
			return true;
		}
		return false;
	}

	function length(){
		return this.listSize;
	}

	function toString(){
		return this.dataStore;
	}

	function insert(element, indexAfter){
		var insertPos = this.find(indexAfter);
		if(insertPos > -1){
			this.dataStore.splice(insertPos+1,0,element);
			++this.listSize;
			return true;
		}
		return false;
	}

	function clear(){
		delete this.dataStore;
		this.dataStore=[];
		this.listSize = this.pos = 0;
	}

	function contains(element){
		for (var i = 0; i < this.dataStore.length; i++) {
			if(this.dataStore[i] === element){
				return true;
			}
			return false;
		};
	}

	function front(){
		this.pos = 0;
	}

	function end(){
		this.pos = this.listSize-1;
	}

	function prev(){
		if(this.pos > 0){
			--this.pos;
		}
	}

	function next(){
		if(this.pos > 0){
			++this.pos;
		}
	}

	function currPos(){
		return this.pos;
	}

	function moveTo(position){
		this.pos = position;
	}

	function getElement(){
		return this.dataStore[this.pos];
	}
}

