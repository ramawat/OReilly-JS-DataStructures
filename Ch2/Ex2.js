/* Store a set of words in an array and display the contents both forward and backward. */

var words = [];

function createArray(sentence){
	if(sentence.indexOf(" ") === -1){
		words[0] = sentence;
	}
	else{
		words = sentence.split(" ");
	}
}
/*-------1st Case--------*/
createArray("Javascript is pretty fun to work with");
console.log(words);
words.reverse();
console.log(words);
//Clearing out the words array
for(var i=words.length-1; i>=0; i--){
  words.pop();    
}

//[ 'Javascript', 'is', 'pretty', 'fun', 'to', 'work', 'with' ]
//[ 'with', 'work', 'to', 'fun', 'pretty', 'is', 'Javascript' ]


/*-------2nd Case--------*/
createArray("Thisisaweirdsentence");
console.log(words);
words.reverse();
console.log(words);

//[ 'Thisisaweirdsentence' ]
//[ 'Thisisaweirdsentence' ]