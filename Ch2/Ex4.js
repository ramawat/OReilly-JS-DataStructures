/* Create an object that stores individual letters in an array and has a function for
displaying the letters as a single word. */

/* Can also be implemented by creating a helper function that would 
 split a word array into an array of chars and passing that to the 
 convertLetters function */

function chartoArr(){
	this.convertLetters = convertLetters;
}

function concat(item1, item2){
	return item1 + item2;
}

function convertLetters(paramArray){
  return paramArray.reduce(concat);
}

var testArr = new chartoArr();
console.log(testArr.convertLetters(['h','e','l','l','o']));
console.log(testArr.convertLetters(['h',' ','e',' ','l',' ','l',' ','o']));
