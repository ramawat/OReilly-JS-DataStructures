/* Modify the weeklyTemps object in the chapter so that it stores a month’s worth of
data using a two-dimensional array. Create functions to display the monthly average,
a specific week’s average, and all the weeks’ averages. */

/* Very Long way of creating a monthly temperature data matrix 
var monthlyTemps = [ 
  [ '73.38','66.68','75.04','82.20','80.60','71.79','71.49','71.05','79.19','73.90','71.86','63.99','70.43','66.49','83.13','78.50',
    '66.87','64.74','64.90','75.67','79.19','68.15','64.43','71.18','80.58','69.06','72.18','70.47','82.23','78.10','78.15' ],
  [ '69.24','75.82','74.89','82.94','73.00','71.33','76.94','83.04','82.67','65.84','78.38','73.16','67.51','68.67','63.65','74.34',
    '68.51','66.40','75.50','80.67','64.08','80.77','73.42','83.06','75.53','66.25','70.02','76.09','79.19','72.96','65.24'],
  [ '70.23','64.58','70.38','78.29','71.67','73.41','77.80','77.46','75.56','72.13','72.65','68.17','64.59','80.20','64.63','79.79',
    '63.05','77.59','83.32','68.72','63.11','75.39','65.16','70.58','69.61','64.92','79.96','82.26','83.20','65.87','79.59']]; */

/* Convenient way of creatig monthly temperature data matrix 
   Returns three month objects with daily temperature readings.
*/
function arrayMatrix (month,days,min,max){
    var testArray = [];
    for(var i=0; i < month; i++){
        testArray[i] = new Array(month);
        var monthly = [];
        for(var j=0; j< days; j++){
            monthly[j] = getRandom(min,max);
        }
        testArray[i] = monthly;
    }
    return testArray;
}

/* Helper random number generator */
function getRandom(min,max){
  return Math.random() * (max - min) + min;
}

function getSum(paramArray){
    var sum =0;
    for(var i=0; i<paramArray.length; i++){
        sum += paramArray[i];
    }
    return sum;
}

function monthStore(){
  this.arrayMatrix = arrayMatrix;
  this.monthAvg = monthAvg;
  this.specWeekAvg = specWeekAvg;
  this.allWeekAvg = allWeekAvg;
}

function monthAvg(paramArrayMatrix){
  var memberMonthSum = 0, returnString="";
  for (var k =0; k<paramArrayMatrix.length; k++){
      var memberMonth = paramArrayMatrix[k];
      memberMonthSum = getSum(memberMonth);
      returnString += "\n Average for Month ".concat(k+1).concat(": ").concat((memberMonthSum/paramArrayMatrix[k].length).toFixed(2));
  }
  return returnString;
}

function countWeeks(paramArray){
    var weekCount =0;
    if(paramArray.length % 7 === 0){
      for(var i=0; i<paramArray.length; i+=7){
          weekCount++;
      }
      return weekCount;
    }
    else{
      var temp = paramArray.length % 7;
      for(var l=0; l<paramArray.length-temp;l+=7){
          weekCount++
      }
      return weekCount+1;
    } 
}

function specWeekAvg(paramArrayMatrix,month, week){
    var weeks=0, membermonth=[], memberWeek=[],
        memberWeekSum=0, weekAvg=0, returnString="";
    
    for(var i=0; i<paramArrayMatrix.length; i++){
      memberMonth = paramArrayMatrix[i];
      memberWeek = paramArrayMatrix[i].chunk(7);
      memberWeekSum = getSum(memberWeek[week-1]);
      weekAvg = memberWeekSum/memberWeek[week-1].length;
      returnString = "\n Average for week ".concat(week).concat(" in month ").concat(month).concat(" is : ").concat(weekAvg.toFixed(2));
    }
    return returnString;
}

Array.prototype.chunk = function(chunkSize) {
    var array = [];
    for (var i=0; i<this.length; i+=chunkSize)
        array.push(this.slice(i,i+chunkSize));
    return array;
}

function allWeekAvg(paramArrayMatrix){
    var weeks = 0, memberMonth=[],memberWeek=[], 
    memberWeekSum=0, weekAvg=0, returnString="";
    
    for (var i=0; i<paramArrayMatrix.length;i++){
        memberMonth = paramArrayMatrix[i];
        weeks += countWeeks(memberMonth);
        memberWeek = paramArrayMatrix[i].chunk(7);
        for(var j=0; j<memberWeek.length;j++){
            memberWeekSum = getSum(memberWeek[j]);
            weekAvg += memberWeekSum/memberWeek[j].length;
        }
        returnString = "\n Average for all weeks ".concat(" : ").concat((weekAvg/(weeks)).toFixed(2));    
    }
    return returnString;
}

var nMonth = 4,
    nDay = 31,
    nMin = 63,
    nMax = 84;

var testMonth = new monthStore();
var newArr = testMonth.arrayMatrix(nMonth,nDay,nMin,nMax);
console.log(testMonth.monthAvg(newArr));
console.log(testMonth.specWeekAvg(newArr,3,4));
console.log(testMonth.allWeekAvg(newArr));

