/*
	Create a grades object that stores a set of student grades in an object. Provide a
function for adding a grade and a function for displaying the student’s grade average.	
*/

function Grades(){
	this.gradeBook = [];
	this.addGrade = addGrade;
	this.dispAvg = dispAvg;
}

function addGrade(grade){
	this.gradeBook.push(grade);
}

function dispAvg(){
	var total = 0;
	for (var i = 0; i < this.gradeBook.length; i++){
		total += this.gradeBook[i];
	}
	return (total/this.gradeBook.length).toFixed(2);
}

var studentGrades = new Grades();
studentGrades.addGrade(85);
studentGrades.addGrade(68);
studentGrades.addGrade(47);
studentGrades.addGrade(88);
studentGrades.addGrade(35);

console.log(studentGrades.dispAvg());
//64.60
